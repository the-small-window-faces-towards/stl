#include<iostream>
#include<algorithm>
#include<vector>
#include<string>
#include<assert.h>
using namespace std;

template<class T>
class MyVector
{
public:
	typedef T* iterator;
	typedef const T* const_iterator;

	//1、构造函数
	MyVector()
		:_start(nullptr)
		,_finish(nullptr)
		,_end_of_storage(nullptr)
	{}

	//2、范围构造函数
	MyVector(iterator first, iterator last)
		:_start(nullptr)
		, _finish(nullptr)
		, _end_of_storage(nullptr)
	{
		while (first != last)
		{
			push_back(*first);
			++first;
		}
	}

	//3、填充构造函数
	MyVector(size_t n, const T& val = T())
		:_start(nullptr)
		,_finish(nullptr)
		,_end_of_storage(nullptr)
	{
		reserve(n);
		for (size_t i = 0; i < n; ++i)
		{
			push_back(val);
		}
	}

	//4、析构函数
	~MyVector()
	{
		delete[]_start;
		_start = _finish = _end_of_storage = nullptr;
	}

	//5、拷贝构造 v2(v1)-->一般写法
	/*MyVector(const MyVector<T>& v)
	{
		_start = new T[v.capacity()];
		_finish = _start;
		_end_of_storage = _start + v.capacity();

		for (size_t i = 0; i < v.size(); ++i)
		{
			*_finish = v[i];
			++_finish;
		}
	}*/

	//6、拷贝构造函数--现在常用写法--开空间+逐个尾插
	MyVector(const MyVector<T>& v)
		:_start(nullptr)
		, _finish(nullptr)
		, _end_of_storage(nullptr)
	{
		reserve(v.capacity());
		for (auto& e : v)
		{
			push_back(e);
		}
	}


	//7、赋值运算符的重载 v3=v1
	//MyVector<T>& operator=(const MyVector<T>& v)
	//{
	//	if (this != &v)
	//	{
	//		//清理空间，让空间变得更干净
	//		delete[]_start;

	//		//申请空间
	//		_start = new T[v.capacity()];

	//		//拷贝数据
	//		for (size_t i = 0; i < v.size(); ++i)
	//		{
	//			_start[i] = v._start[i];
	//		}

	//		//更新数组的大小及容量
	//		_finish = _start + v.size();
	//		_end_of_storage = _start + v.capacity();
	//	}
	//	return *this;
	//}

	//8、赋值运算符的重载 v3 = v1
	MyVector<T>& operator=(MyVector<T> v)
	{
		swap(v);

		return *this;
	}
	//9、swap
	void swap(MyVector<T>& v)
	{
		::swap(_start, v._start);
		::swap(_finish, v._finish);
		::swap(_end_of_storage, v._end_of_storage);
	}

	//10、reserve函数
	void reserve(size_t n)
	{
		if (n > capacity())
		{
			size_t sz = size();
			T* tmp = new T[n];

			if (_start)
			{
				for (size_t i = 0; i < sz; ++i)
				{
					tmp[i] = _start[i];
				}
				delete[]_start;
			}

			_start = tmp;
			_finish = tmp + sz;
			_end_of_storage = tmp + n;
		}
	}

	//7、resize--void resize (size_type n, value_type val = value_type());
	void resize(size_t n, const T &val = T())
	{
		if (n < size())
		{
			_finish = _start + n;
		}
		else
		{
			if (n > capacity())
			{
				reserve(n);
			}

			while (_finish<_start+n)
			{
				*_finish = val;
				++_finish;
			}
		}
	}

	//8、push_back
	void push_back(const T& val)
	{
		if (_finish == _end_of_storage)
		{
			size_t newCapacity = capacity() == 0 ? 2 : 2 * capacity();
			reserve(newCapacity);
		}

		*_finish = val;
		++_finish;
	}

	

	//9、pop_back
	void pop_back()
	{
		assert(_start < _finish);
		--_finish;
	}


	//10、在位置pos上插入数据--insert
	void insert(iterator pos, const T& val)
	{
		assert(pos <= _finish);

		if (_finish == _end_of_storage)
		{
			size_t n = pos - _start;
			size_t newCapacity = capacity() == 0 ? 2 : 2 * capacity();
			reserve(newCapacity);

			pos = _start + n;
		}

		iterator it = _finish;
		while (it >= pos)
		{
			*it = *(it - 1);
			--it;
		}

		*pos = val;
		++_finish;
	}

	//11、删除位置pos上的数据
	iterator erase(iterator pos)
	{
		assert(pos < _finish);

		iterator it = pos;
		while (it < _finish - 1)
		{
			*it = *(it + 1);
			++it;
		}
		--_finish;
		return pos;
	}


	iterator begin()
	{
		return _start;
	}

	iterator end()
	{
		return _finish;
	}

	const_iterator begin() const
	{
		return _start;
	}

	const_iterator end() const
	{
		return _finish;
	}

	
	size_t size() const
	{
		return _finish - _start;
	}

	size_t capacity() const
	{
		return _end_of_storage - _start;
	}

	T& operator[](size_t index)
	{
		assert(index < size());
		return _start[index];
	}

	const T& operator[](size_t i)const
	{
		assert(i < size());

		return _start[i];
	}

private:
	iterator _start;//表示目前使用空间的头
	iterator _finish;//表示目前使用空间的尾
	iterator _end_of_storage;//表示目前可用空间的尾
};

void print_vector(const MyVector<int>& v)
{
	MyVector<int>::const_iterator it = v.begin();
	while (it != v.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}

//构造函数测试
void test01()
{
	//1、无参构造函数
	MyVector<int>v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);
	v1.push_back(5);
	print_vector(v1);

	//2、范围构造函数
	MyVector<int>v2(v1.begin(), v1.end());
	print_vector(v2);

	//3、填充构造函数
	MyVector<int>v3(5, 10);
	print_vector(v3);
}



//拷贝构造函数测试
void test02()
{
	MyVector<string>v1;
	v1.push_back("abcd");
	v1.push_back("1234");
	for (int i = 0; i < v1.size(); ++i)
	{
		cout << v1[i] << " ";
	}

	MyVector<string> v2(v1);
	for (int i = 0; i < v1.size(); ++i)
	{
		cout << v2[i] << " ";
	}
}

//赋值运算符重载测试
void test03()
{
	MyVector<string>v1;
	v1.push_back("abc");
	v1.push_back("123");
	for (int i = 0; i < v1.size(); ++i)
	{
		cout << v1[i] << " ";
	}
	cout << endl;

	MyVector<string>v2;
	v2 = v1;
	for (int i = 0; i < v2.size(); ++i)
	{
		cout << v2[i] << " ";
	}
	cout << endl;
}

//reserve、resize函数测试
void test04()
{
	MyVector<int>v1;
	v1.reserve(10);
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);
	print_vector(v1);
	cout << v1.size() << endl; 
	cout << v1.capacity() << endl; 

	v1.resize(20,100);
	print_vector(v1);
	cout << v1.size() << endl;
	cout << v1.capacity() << endl;

	v1.resize(6, 8);
	print_vector(v1);
	cout << v1.size() << endl;
	cout << v1.capacity() << endl;
}

//push_back、pop_back函数测试
void test05()
{
	MyVector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	print_vector(v);

	v.pop_back();
	v.pop_back();
	print_vector(v);
}

//insert和erase函数测试
void test06()
{
	MyVector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	cout << v.size() << endl;
	cout << v.capacity() << endl;
	print_vector(v);

	v.insert(v.begin() + 1, 10);
	cout << v.size() << endl;
	cout << v.capacity() << endl;
	print_vector(v);

	v.erase(v.begin() + 1);
	cout << v.size() << endl;
	cout << v.capacity() << endl;
	print_vector(v);
}


int main()
{
	test06();
	
	return 0;
}
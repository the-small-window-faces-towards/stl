#include<iostream>
#include<queue>
#include<list>


//队列
//C++ 标准库中的基本用法
void test_queue()
{
	std::queue<int> myqueue;

	//1、push--在队尾将元素i入队列
	for (int i = 0; i < 10; ++i)
		myqueue.push(i);

	//2、size--返回队列元素的个数
	std::cout << myqueue.size() << std::endl;

	//3、emplace--在队列的末尾添加一个新元素，该元素在当前队列最后一个元素的后面
	myqueue.emplace(11);

	std::cout << myqueue.size() << std::endl;

	//4、empty()--判断队列是否为空
	//5、front()--返回队头元素的引用
	//6、back()--返回队尾元素的引用
	//6、pop()--将队头元素出队列
	while (!myqueue.empty())
	{
		//返回队头的数据
		std::cout << myqueue.front() << " ";
		myqueue.pop();
	}
	std::cout << std::endl;

	std::cout << myqueue.size() << std::endl;
}

//新增一个容器的模板参数，T是数据的类型，
//Container是一个容器的类型(容器具体是什么类型，可以根据需要传入)。
template<class T, class Container=std::deque<T>>
class MyQueue
{
public:
	//1、将元素添加队尾
	void push(const T& val)
	{
		_con.push_back(val);
	}

	//2、移除队头元素
	void pop()
	{
		if (!empty())
		{
			_con.pop_front();
		}
		else
		{
			throw std::runtime_error("Queue is empty.");
		}
	}

	//3、访问队头元素的引用
	T& front()
	{
		if (!empty())
		{
			return _con.front();
		}
		else
		{
			throw std::runtime_error("Queue is empty.");
		}
	}

	//4、访问队尾元素的引用
	T& back()
	{
		if (!empty())
		{
			return _con.back();
		}
		else
		{
			throw std::runtime_error("Queue is empty.");
		}
	}


	//5、返回队列的大小
	size_t size()
	{
		return _con.size();
	}

	//6、检查队列是否为空
	bool empty()
	{
		return _con.empty();
	}

	

private:
	Container _con;
};


void test_MyQueue()
{
	//MyQueue<int, vector<int>> q;//不能用vector，因为vector没有提供pop_front接口
	MyQueue<int, std::list<int>> queue;
	queue.push(1);
	queue.push(2);
	queue.push(3);
	queue.push(4);

	while (!queue.empty())
	{
		std::cout << queue.front() << " ";
		queue.pop();
	}
	std::cout << std::endl;
}

int main()
{
	test_queue();
	test_MyQueue();


	return 0;
}
#include<iostream>
#include<list>
#include<vector>
using namespace std;

//1、list的构造函数
void test01()
{
	list<int>l1;//构造一个空对象l1
	list<int>l2(4, 100);//链表l2中放入4个100
	list<int>l3(l2.begin(), l2.end());//用l2的[begin(),end() )左开右闭的区间构造l3
	list<int>l4(l3);//使用l3拷贝构造l4

	//使用数组作为迭代器区间构造链表l5
	int array[] = { 12,3,66,88 };
	list<int>l5(array, array + sizeof(array) / sizeof(int));

	//列表格式初始化C++11
	list<int>l6{ 2,4,6,8,10,12 };

	//用迭代器方式打印l5中的元素
	list<int>::iterator it = l5.begin();
	while (it != l5.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	//范围for遍历--C++11
	for (auto e : l5)
	{
		cout << e << " ";
	}
	cout << endl;
}

//2、list的迭代器
//注意：链表的遍历只能用迭代器和范围for
void PrintList(const list<int>& l)
{
	list<int>::const_iterator it = l.begin();
	while (it != l.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}

void test02()
{
	//1、使用数组作为迭代器区间构造链表l
	int array[] = { 12,33,45,67 };
	list<int>l(array, array+sizeof(array) / sizeof(array[0]));

	//2、使用正向迭代器正向遍历链表list中的元素
	//list<int>::iterator it = l.begin(); //c++98中的语法
	auto it = l.begin();//c++11之后的推荐写法
	while (it != l.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	//3、使用反向迭代器逆向打印l中的数据
	//list<int>::reverse_iterator rit = l.rbegin();
	auto rit = l.rbegin();
	while (rit != l.rend())
	{
		cout << *rit << " ";
		++rit;
	}
	cout << endl;
}

//3、list的插入和删除
//push_back/pop_back/push_front/pop_front
void test03()
{
	int array[] = { 12,13,14 };
	list<int>l(array, array + sizeof(array) / sizeof(array[0]));
	PrintList(l);
	
	//在list的尾部插入55，头部插入11
	l.push_back(55);
	l.push_front(11);
	PrintList(l);

	//删除list的尾结点和头结点
	l.pop_back();
	l.pop_front();
	PrintList(l);
}

//4、在指定位置插入和删除结点
//insert/erase
void test04()
{
	int array[] = { 1,2,3 };
	list<int>l(array, array + sizeof(array) / sizeof(array[0]));

	//获取list中第二个结点
	auto pos = ++l.begin();
	cout << *pos << endl;

	//在pos前插入值为4的元素
	l.insert(pos,4);
	PrintList(l);

	//在pos前插入6个值为8的元素
	l.insert(pos, 6, 8);
	PrintList(l);

	//在pos前插入[v.begin(),v.end() )区间中的元素
	vector<int>v{ 10,11,12 };
	l.insert(pos, v.begin(), v.end());
	PrintList(l);

	//删除pos位置上的元素
	l.erase(pos);
	PrintList(l);

	//删除list中[begin(),end() )区间中的元素，这里即指删除list中的所有元素
	l.erase(l.begin(), l.end());
	PrintList(l);
}

//5、
//size：返回list中有效结点的个数
//swap：交换两个list中的元素
//clear：清空list中的有效元素
//empty：检测list是否为空，是返回true，否则返回false
void test05()
{
	int array[] = { 11,12,13,14,15 };
	list<int>l1(array, array + sizeof(array) / sizeof(array[0]));
	PrintList(l1);

	//交换l1和l2中的元素
	list<int>l2(2, 100);
	l1.swap(l2);
	PrintList(l1);
	PrintList(l2);

	//使用clear函数将l2中的元素清空
	//使用size函数返回l1和l2中有效结点的个数
	l2.clear();
	cout << l2.size() << endl;
	cout << l1.size() << endl;

	//使用empty函数返回链表是否为空
	list<int>l3;
	int sum(0);

	for (int i = 0; i <=10; ++i)
		l3.push_back(i);

	while (!l3.empty())
	{
		sum += l3.front();
		l3.pop_front();
	}
	cout << "total:" << sum << endl;
}

//6、front：返回list的第一个结点中值的引用
//   back：返回list的最后一个结点中值的引用
void test06()
{
	//front
	list<int>mylist;

	mylist.push_back(77);
	mylist.push_back(22);

	mylist.front() -= mylist.back();
	cout << "mylist.front() is now " << mylist.front() << endl;

	//back
	mylist.push_back(10);

	while (mylist.back() != 0)
	{
		mylist.push_back(mylist.back() - 1);
	}

	cout << "mylist contains: ";
	for (list<int>::iterator it = mylist.begin(); it != mylist.end(); ++it)
		cout << *it <<" ";
	cout << endl;
}


int main()
{
	//test01();
	//test02();
	//test03();
	//test04();
	//test05();
	test06();


	return 0;
}
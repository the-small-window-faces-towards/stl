#include<iostream>
#include<vector>
#include<algorithm>
#include<assert.h>
using namespace std;

//1、vector的构造函数
void test01()
{
	//1、构造函数
	//无参构造
	vector<int> v1;
	//无参构造可以通过push_back函数进行尾插
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);
	//打印
	for (size_t i = 0; i < v1.size(); ++i)
	{
		cout << v1[i] << " ";
	}
	cout << endl;

	//构造并初始化值为100的4个整数
	vector<int>v2(4, 100);
	//打印
	for (size_t i = 0; i < v2.size(); ++i)
	{
		cout << v2[i] << " ";
	}
	cout << endl;

	//使用迭代器进行初始化构造
	vector<int>v3(v2.begin(), v2.end());
	//打印
	for (size_t i = 0; i < v3.size(); ++i)
	{
		cout << v3[i] << " ";
	}
	cout << endl;

	//拷贝构造
	vector<int> v4(v1);
	//打印
	for (size_t i = 0; i < v4.size(); ++i)
	{
		cout << v4[i] << " ";
	}
	cout << endl;
}

//2、vector的3种遍历方式
void test02()
{
	vector<int> v1;
	//无参构造可以通过push_back函数进行尾插
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);

	//2、vector的3种遍历方式
	//(1)for循环+size()+[]-->遍历修改数据
	for (size_t i = 0; i < v1.size(); ++i)
	{
		v1[i] *= 2;
		cout << v1[i] << " ";
	}
	cout << endl;

	//(2)普通正向迭代器  可读可写
	vector<int>::iterator it = v1.begin();
	while (it != v1.end())
	{
		*it += 1;
		cout << *it << " ";
		++it;
	}
	cout << endl;

	//(3)范围for--被编译器替换成迭代器的方式遍历支持的
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;
}

//3、3种vector iterator的使用
void print_vector(const vector<int>& v)
{
	vector<int>::const_iterator it = v.begin();
	while (it != v.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}

void test03()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);

	//1、正向普通迭代器-->可读可写
	vector<int>::iterator it = v.begin();
	while (it != v.end())
	{
		*it *= 2;
		cout << *it << " ";
		++it;
	}
	cout << endl;

	//2、const迭代器
	print_vector(v);

	//3、反向迭代器
	vector<int>:: reverse_iterator rit = v.rbegin();
	while (rit != v.rend())
	{
		cout << *rit << " ";
		++rit;
	}
	cout << endl;
}


//4、vector的空间增长
void test04()
{
	//1、获取数据的个数--size
	//2、获取容量大小--capacity
	vector<int> myvector;
	for (int i = 0; i < 10; ++i)
	{
		myvector.push_back(i);
	}
	cout << "size: " << myvector.size() << endl;//10
	cout << "capacity: " << myvector.capacity() << endl;//13
	for (int i = 0; i < myvector.size(); ++i)
	{
		cout << myvector[i] << " ";
	}
	cout << endl;

	//3、判断是否为空--empty
	int sum = 0;
	while (!myvector.empty())
	{
		sum += 1;
		myvector.pop_back();
	}
	cout << sum << endl;

	//4、改变vector的size--resize
	//void resize (size_type n, value_type val = value_type());
	myvector.resize(5);
	myvector.resize(8, 100);
	myvector.resize(12);

	for (int i = 0; i < myvector.size(); ++i)
	{
		cout << myvector[i] << " ";
	}
	cout << endl;
	cout << "==========="<<endl;

	//5、改变vector的capacity
	vector<int>::size_type sz;

	vector<int>foo;
	sz = foo.capacity();
	cout << "making foo grow:\n";
	for (int i = 0; i < 100; ++i)
	{
		foo.push_back(i);
		if (sz != foo.capacity())
		{
			sz = foo.capacity();
			cout << "capacity changed:" << sz << endl;
		}
	}

	vector<int>bar;
	sz = bar.capacity();
	bar.reserve(100);
	cout << "making bar grow:\n";
	for (int i = 0; i < 100; ++i)
	{
		bar.push_back(i);
		if (sz != bar.capacity())
		{
			sz = bar.capacity();
			cout << "capacity changed:" << sz << endl;
		}
	}
}

//5、vector的增删查改
void test05()
{
	//1、尾插--> push_back
	//2、尾删
	vector<int> myvector;
	int sum(0);
	myvector.push_back(100);
	myvector.push_back(200);
	myvector.push_back(300);
	while (!myvector.empty())
	{
		sum += myvector.back();
		myvector.pop_back();
	}
	cout << "sum:"<<sum << endl;

	//3、任意位置插入和删除 insert(在position位置之前插入val)
	//   erase(删除position位置的数据)
	//4、find 查找，这个是算法模块实现，不是vector的成员接口
	//使用列表方式初始化，c++11新语法
	vector<int>myvector2 = { 1,2,3,4,5 };

	//在指定位置前插入值为val的元素，比如在4之前插入50，如果没有则不插入
	//1.先使用find查找4所在的位置
	auto pos = find(myvector2.begin(), myvector2.end(), 4);
	if (pos != myvector2.end())
	{
		myvector2.insert(pos,50);
	}

	vector<int>::iterator it = myvector2.begin();
	while (it != myvector2.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	//2.删除pos位置的数据
	pos = find(myvector2.begin(), myvector2.end(), 3);
	myvector2.erase(pos);
	it = myvector2.begin();
	while (it != myvector2.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	//5、swap函数--交换两个vector的数据空间
	//6、operator[]像数组一样访问
	vector<int>foo(3, 100);
	vector<int>bar(5, 200);
	
	foo.swap(bar);
	cout << "foo contains:";
	for (int i = 0; i < foo.size(); ++i)
	{
		cout << foo[i] << " ";
	}
	cout << endl;

	cout << "bar contains:";
	for (int i = 0; i < bar.size(); ++i)
	{
		cout << bar[i] << " ";
	}
	cout << endl;
}

//6、迭代器的失效问题
//(1)迭代器失效情况1：
void test06()
{
	vector<int>v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);

	vector<int>::iterator it = v.begin();

	v.push_back(6);
	v.push_back(7);

	while (it != v.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}

//(2)迭代器失效情况2：
void test07()
{
	vector<int>v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);

	//要求删除容器中的所有偶数
	vector<int>::iterator it = v.begin();
	while (it != v.end())
	{
		if (*it % 2 == 0)
		{
			v.erase(it);
		}
		++it;
	}

	for (auto e : v)
	{
		cout << e << endl;
	}
	cout << endl;
}

//迭代器失效情况2的解决办法
void test08()
{
	vector<int>v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);

	vector<int>::iterator it = v.begin();
	while (it != v.end())
	{
		if (*it % 2 == 0)
		{
			it = v.erase(it);
		}
		else
		{
			++it;
		}
	}

	for (auto e : v)
	{
		cout << e << endl;
	}
	cout << endl;
}



int main()
{
	/*test01();
	test02();*/
	//test03();
	//test04();
	//test05();
	//test06();//该代码会报错
	//test07();//该代码会报错vector<int>v;
	test08();


	return 0;
}


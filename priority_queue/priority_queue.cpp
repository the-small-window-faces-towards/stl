#include<vector>
#include<queue>
#include<iostream>
#include<algorithm>//用于 std::make_heap, std::push_heap, std::pop_heap

//priority_queue C++标准库的基本用法
void test_priority_queue()
{
	std::priority_queue<int> mypq;

	//1、push(val)--在优先级队列中插入元素val
	for (int i = 0; i < 10; ++i)
		mypq.push(i);

	//2、empty()--检测优先级队列是否为空，是返回true，否则返回false
	//3、删除优先级队列中最大（最小）元素，即堆顶元素
	while (!mypq.empty())
	{
		std::cout << mypq.top() << " ";
		mypq.pop();
	}
	std::cout << std::endl;
}

//实现priority_queue
template<class T, class Container=std::vector<T>>
class MyPriorityQueue
{
public:
	//1、默认构造函数
	MyPriorityQueue() {}

	//2、构造函数，可以指定底层容器
	MyPriorityQueue(const Container& c)
		:_con(c)
	{
		//std::make_heap(_con.begin(), _con.end());
		//借助向下调整算法，将底层容器调整成堆
		//使用方法1的调整方式--从第一个非叶子结点开始调整
		for (int i = (_con.size() - 1) / 2; i >= 0; --i)
			AdjustDown_1(i);

		//使用方法2的调整方式--先调整左右子树其中的一个
		//for (int i = (_con.size() / 2) - 1; i >= 0; --i)
			//AdjustDown_2();
	}

	//3、将元素添加到优先队列中
	void push(const T& val)
	{
		_con.push_back(val);
		//std::push_heap(_con.begin(), _con.end());
		//AdjustUp_1(_con.size() - 1);
		AdjustUp_2();
	}

	//4、弹出优先队列中的最大元素
	void pop()
	{
		if (!empty())
		{
			//std::pop_heap(_con.begin(), _con.end());
			//1、先将堆顶元素与堆数组中末尾的元素进行交换
			std::swap(_con[0], _con[_con.size() - 1]);
			//2、此时，弹出堆数组末尾的元素，即堆中最大的元素
			_con.pop_back();
			//3、使用向下调整算法，调整建堆
			//AdjustDown_1(0);
			AdjustDown_2();
		}
		else
		{
			throw std::runtime_error("Priority queue is empty.");
		}
	}

	//5、访问优先队列中的最大元素
	T& top()
	{
		if (!empty())
		{
			return _con[0];
		}
		else
		{
			throw std::runtime_error("Priority queue is empty.");
		}
	}

	//6、检查优先队列是否为空
	bool empty() const
	{
		return _con.empty();
	}
	
	//7、返回优先队列的大小
	size_t size() const
	{
		return _con.size();
	}

private:
	Container _con;//使用底层元素存储优先队列的元素

	//向上调整算法--写法1
	void AdjustUp_1(int child)
	{
		int parent = (child - 1) / 2;

		while (child>0)
		{
			if (_con[parent] < _con[child])
			{
				std::swap(_con[parent], _con[child]);
				child = parent;
				parent = (child - 1) / 2;
			}
			else
			{
				break;
			}
		}
	}

	//向上调整算法--写法2
	void AdjustUp_2()
	{
		int child = _con.size()-1;

		while (child > 0)
		{
			int parent = (child - 1) / 2;
			if (_con[child] > _con[parent])
			{
				std::swap(_con[child], _con[parent]);
				child = parent;
			}
			else
			{
				break;
			}
		}
	}

	//向下调整算法--写法1
	void AdjustDown_1(int root)
	{
		int parent = root;
		int child = root * 2 + 1;//默认左孩子节点的数值大于右孩子节点的数值

		while (child<_con.size())
		{
			if (child + 1 < _con.size() && _con[child] < _con[child + 1])
			{
				child++;
			}

			if (_con[parent] < _con[child])
			{
				std::swap(_con[parent], _con[child]);
				parent = child;
				child = parent * 2 + 1;
			}
			else
			{
				break;
			}
		}
	}

	//向下调整算法--写法2
	void AdjustDown_2()
	{
		int parent = 0;
		int size = _con.size();

		while (true)
		{
			int leftChild = 2 * parent + 1;
			int rightChild = 2 * parent + 2;
			int largest = parent;

			if (leftChild < size && _con[largest] < _con[leftChild])
				largest = leftChild;

			if (rightChild < size && _con[largest] < _con[rightChild])
				largest = rightChild;

			if (largest != parent)
			{
				std::swap(_con[parent], _con[largest]);
				parent = largest;
			}
			else
			{
				break;
			}
		}
	}
};


void MyPriorityQueue_Test()
{
	// 使用默认底层容器（std::vector）的示例
	MyPriorityQueue<int> mypq;
	mypq.push(3);
	mypq.push(1);
	mypq.push(4);
	mypq.push(1);

	while (!mypq.empty())
	{
		std::cout << mypq.top() << " ";
		mypq.pop();
	}
	std::cout << std::endl;
}


int main()
{
	//test_priority_queue();
	MyPriorityQueue_Test();

	return 0;
}
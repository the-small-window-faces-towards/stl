#pragma once
#include<iostream>

using namespace std;

enum Colour
{
	BLACK,
	RED,
};


template<class T>
struct RBTreeNode
{
	RBTreeNode<T>* _left;//节点的左孩子
	RBTreeNode<T>* _right;//节点的右孩子
	RBTreeNode<T>* _parent;//节点的父亲节点(红黑树需要旋转，为了实现简单给出该字段）

	T _data;//节点的值域，_data可能是一个key值，也可能是一个K-V键值对

	Colour _col;//节点的颜色

	RBTreeNode(const T& data)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _data(data)
		, _col(RED)
	{}
};

template<class T,class Ref,class Ptr>
struct __TreeIterator
{
	typedef RBTreeNode<T> Node;
	typedef __TreeIterator<T,Ref,Ptr> Self;
	Node* _node;

	//节点指针构造迭代器
	__TreeIterator(Node* node)
		:_node(node)
	{}

	//使用普通迭代器构造const迭代器的构造函数
	__TreeIterator(const __TreeIterator<T, Ref, Ptr>& it)
		:_node(it._node)
	{}

	Ref operator*()
	{
		return _node->_data;
	}

	Ptr operator->()
	{
		return &_node->_data;
	}

	bool operator!=(const Self& s)
	{
		return _node != s._node;
	}

	bool operator==(const Self& s)
	{
		return _node == s._node;
	}

	Self& operator++()
	{
		//1、如果节点的右子树不为空，中序的下一个节点就是右子树的最左节点
		//2、如果右为空，表示_node所在的子树已经访问完成，下一个节点在它的祖先中去找；
		//
		if (_node->_right)
		{
			Node* subLeft = _node->_right;
			while (subLeft->_left)
			{
				subLeft = subLeft->_left;
			}

			_node = subLeft;
		}
		else
		{
			Node* cur = _node;
			Node* parent = cur->_parent;
			while (parent&&cur==parent->_right)
			{
				cur = cur->_parent;
				parent = parent->_parent;
			}
			_node = parent;
		}

		return *this;
	}

	Self& operator--()
	{

	}
};

template<class K, class T,class KOfT>
class RBTree
{
	typedef RBTreeNode<T> Node;
public:
	typedef __TreeIterator<T,T&,T*> iterator;
	typedef __TreeIterator<T,const T&,const T*> const_iterator;

	iterator begin()
	{
		Node* cur = _root;
		while (cur && cur->_left)
		{
			cur = cur->_left;
		}

		return iterator(cur);
	}

	iterator end()
	{
		return iterator(nullptr);
	}

	//1、红黑树插入节点
	pair<iterator,bool> Insert(const T& data)
	{
		//1、按二叉搜索树的规则插入节点
		//如果二叉树为空，则将新插入的节点作为根节点
		if (_root == nullptr)
		{
			_root = new Node(data);
			_root->_col = BLACK;//红黑树的根节点为黑色
			return make_pair(iterator(_root),true);
		}

		KOfT koft;
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (koft(data) > koft(cur->_data))
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (koft(data) < koft(cur->_data))
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return make_pair(iterator(cur), false);
			}
		}

		cur = new Node(data);
		Node* newnode = cur;
		if (koft(cur->_data) > koft(parent->_data))
		{
			parent->_right = cur;
			cur->_parent = parent;
		}
		else if (koft(cur->_data) < koft(parent->_data))
		{
			parent->_left = cur;
			cur->_parent = parent;
		}

		//cur->_col = RED;
		//这里默认新插入的节点是红色节点，为什么呢？
		//因为插入新节点时，就涉及破坏规则2(红黑树中没有连续的红节点);还是规则3(红黑树每条路径都有相同数量的黑节点)。
		//首先，插入红色节点时，不一定会破坏规则2(如果插入节点的父亲节点是黑色节点)；即使破坏了规则2，新插入的节点是
		//红色节点也只会影响一条路径。
		//如果新插入的节点是黑色的，会影响二叉树的所有路径，因为红黑树的每条路径都要有相同数量的黑色节点。

		//情况1：cur节点为红色、parent节点为红色、grandfather为黑色、uncle节点存在且为红色；
		//情况2：uncle节点不存在
		//情况3：uncle节点存在且为黑
		while (parent && parent->_col == RED)
		{
			//cur节点的父亲节点parent是红色，此时parent节点不可能是根节点
			//此时看cur节点的叔叔节点uncle的颜色。
			Node* grandfather = parent->_parent;
			if (grandfather->_left == parent)
			{
				Node* uncle = grandfather->_right;
				if (uncle && uncle->_col == RED)
				{
					parent->_col = uncle->_col = BLACK;
					grandfather->_col = RED;

					//如果grandfather不是根节点，继续往上处理。
					cur = grandfather;
					parent = cur->_parent;
				}
				else
				{
					//情况3：双旋；先parent节点左旋，转换为情况2
					if (parent->_right == cur)
					{
						RotateL(parent);
						swap(parent, cur);
					}

					//情况2：情况2也可能是情况3进行左单旋之后，需要再进行右单旋
					RotateR(grandfather);
					grandfather->_col = RED;
					parent->_col = BLACK;

					break;
				}
			}
			else
			{
				Node* uncle = grandfather->_left;
				//情况1：uncle存在、且为红
				//情况2 or 情况3：uncle不存在 or uncle存在、且为黑
				if (uncle && uncle->_col == RED)
				{
					parent->_col = uncle->_col = BLACK;
					grandfather->_col = RED;

					//如果grandfather不是根节点，继续往上处理
					cur = grandfather;
					parent = cur->_parent;
				}
				else
				{
					//情况3
					if (cur == parent->_left)
					{
						RotateR(parent);
						swap(cur, parent);
					}

					//情况2
					RotateL(grandfather);

					grandfather->_col = RED;
					parent->_col = BLACK;
				}
			}
		}

		//最终将根节点变为黑色
		_root->_col = BLACK;

		return make_pair(iterator(newnode), true);
	}

	

	//2、红黑树查找节点
	iterator Find(const T& data)
	{
		KOfT koft;
		Node* cur = _root;
		while (cur)
		{
			if (koft(data) > koft(cur->_data))
			{
				cur = cur->_right;
			}
			else if (koft(data) < koft(cur->_data))
			{
				cur = cur->_left;
			}
			else
			{
				return iterator(cur);
			}
		}

		return iterator(nullptr);
	}

	//中序遍历
	void InOrder()
	{
		_InOrder(_root);
	}

private:
	void _InOrder(Node* root)
	{
		if (root == nullptr)
			return;

		_InOrder(root->_left);
		cout << root->_kv.first << ":" << root->_kv.second << endl;
		_InOrder(root->_right);
	}

	//左单旋
	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		parent->_right = subRL;
		if (subRL)
			subRL->_parent = parent;

		subR->_left = parent;
		Node* grandfather = parent->_parent;
		parent->_parent = subR;

		//如果原来parent是这棵树的根节点，左旋转完成后subR节点变成这棵树的根节点
		if (grandfather == nullptr)
		{
			_root = subR;
			subR->_parent = nullptr;
		}
		else
		{
			if (parent == grandfather->_left)
			{
				grandfather->_left = subR;
			}
			else if (parent == grandfather->_right)
			{
				grandfather->_right = subR;
			}

			subR->_parent = grandfather;
		}
	}

	//右单旋
	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;

		subL->_right = parent;
		Node* grandfather = parent->_parent;
		parent->_parent = subL;

		if (grandfather == nullptr)
		{
			_root = subL;
			subL->_parent = nullptr;
		}
		else
		{
			if (grandfather->_left == parent)
			{
				grandfather->_left = subL;
			}
			else if (grandfather->_right == parent)
			{
				grandfather->_right = subL;
			}

			subL->_parent = grandfather;
		}
	}

	Node* _root = nullptr;
};




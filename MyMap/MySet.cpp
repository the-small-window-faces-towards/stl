#include"RBTree.h"

template<class K>
class MySet
{
	struct SetKeyOfT
	{
		const K& operator()(const K& k)
		{
			return k;
		}
	};

public:
	typedef typename RBTree<K, K, SetKeyOfT>::iterator iterator;

	iterator begin()
	{
		return _t.begin();
	}

	iterator end()
	{
		return _t.end();
	}

	pair<iterator, bool> Insert(const K& k)
	{
		return _t.Insert(k);
	}

private:
	RBTree<K, K,SetKeyOfT> _t;
};

void test_set()
{
	MySet<int> s;
	
	int a[] = { 4,25,6,1,3,5,15,7,16,14 };
	for (auto e : a)
	{
		s.Insert(e);
	}

	MySet<int>::iterator it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	for (auto e : s)
	{
		//e = 1;
		cout << e << " ";
	}
	cout << endl;
}

int main()
{
	test_set();

	return 0;
}


#include"RBTree.h"

template<class K, class V>
class MyMap
{
	struct MapKeyOfT
	{
		const K& operator()(const pair<const K, V>& kv)
		{
			return kv.first;
		}
	};

public:
	typedef typename RBTree<K, pair<const K,V>, MapKeyOfT>::iterator iterator;

	iterator begin()
	{
		return _t.begin();
	}

	iterator end()
	{
		return _t.end();
	}

	pair<iterator,bool> Insert(const pair<const K, V>& kv)
	{
		return _t.Insert(kv);
	}

	//[]返回key对应的value值
	V& operator[](const K& key)
	{
		//这里的插入操作可能会成功，也可能会失败，如果key已经存在则失败；
		//无论是插入成功还是插入失败，都会返回节点对应的迭代器，所以就能拿到该节点对应的value
		//V()是缺省值，如果插入的是int即为0；如果是string，那么就构造一个空字符串对象
		pair<iterator, bool> ret = _t.Insert(make_pair(key, V()));
		return ret.first->second;
	}

private:
	RBTree<K, pair<const K, V>,MapKeyOfT> _t;
};


void test_map_1()
{
	MyMap<string, string> dict;
	dict.Insert(make_pair("sort", "排序"));
	dict.Insert(make_pair("string", "字符串"));
	dict.Insert(make_pair("count", "计数"));
	dict.Insert(make_pair("stirng", "(字符串)"));

	MyMap<string, string>::iterator it = dict.begin();
	while (it != dict.end())//it->返回一个指向pair的指针
	{
		cout << it->first << ":" << it->second << endl;
		++it;
	}
	cout << endl;

	MyMap<string, string>::iterator rit = dict.begin();
	while (rit != dict.end())//*rit返回一个pair
	{
		cout << (*rit).first << ":" << (*rit).second << endl;
		++rit;
	}
	cout << endl;

	for (auto& kv : dict)
	{
		cout << kv.first << ":" << kv.second << endl;
	}
	cout << endl;
}


void test_map_2()
{
	string arr[] = { "西瓜","西瓜","苹果","桃子","西瓜", "李子","李子" };
	MyMap<string, int>countMap;

	for (auto& e : arr)
	{
		countMap[e]++;
	}

	for (auto& kv : countMap)
	{
		cout << kv.first << ":" << kv.second << endl;
	}
	cout << endl;
}


int main()
{
	test_map_2();

	return 0;
}


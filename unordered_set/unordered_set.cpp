#include"HashTable.h"

template<class K, class Hash = _Hash<K>>
class MyUnorderedSet
{
	struct MapKeyOfT
	{
		const K& operator()(const K& key)
		{
			return key;
		}
	};

public:
	typedef typename HashTable<K, K, MapKeyOfT, Hash>::const_iterator iterator;
	typedef typename HashTable<K, K, MapKeyOfT, Hash>::const_iterator const_iterator;

	iterator begin() 
	{
		return _ht.begin();
	}

	iterator end() 
	{
		return _ht.end();
	}
	
	const_iterator begin() const
	{
		return _ht.begin();
	}

	const_iterator end() const
	{
		return _ht.end();
	}

	pair<iterator,bool> Insert(const K& key)
	{
		//写法1：
		//1、先用一个普通迭代器来接收
		/*pair<typename HashTable<K, K, MapKeyOfT, Hash>::iterator, bool>ret = _ht.Insert(key);
		//2、再用普通迭代器来构造const迭代器，前提是补充实现一个拷贝构造函数。
		return pair<iterator, bool>(ret.first, ret.second);*/

		//写法2：该写法会将_ht.Insert(key)的返回值赋值给pair<iterator,bool>，该过程会自动调用拷贝构造函数
		return _ht.Insert(key);
	}

private:
	HashTable<K, K, MapKeyOfT, Hash> _ht;
};

void test_MyUnorderedSet()
{
	MyUnorderedSet<int> MySet;
	MySet.Insert(3);
	MySet.Insert(1);
	MySet.Insert(3);
	MySet.Insert(4);
	MySet.Insert(5);
	MySet.Insert(0);
	

	auto it = MySet.begin();
	while (it != MySet.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}


int main()
{
	test_MyUnorderedSet();
	return 0;
}





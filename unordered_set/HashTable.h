#pragma once
#include<iostream>
#include<vector>
#include<string>
using namespace std;

template<class T>
struct HashNode
{
	T _data;
	HashNode<T>* _next;

	HashNode(const T& data)
		:_data(data)
		, _next(nullptr)
	{}
};

//前置声明
template<class K, class T, class KeyOfT, class Hash>
class HashTable;

template<class K, class T, class Ref, class Ptr, class KeyOfT, class Hash>
struct __HashTableIterator
{
	KeyOfT keyoft;
	typedef __HashTableIterator<K, T, Ref, Ptr, KeyOfT, Hash> Self;
	typedef __HashTableIterator<K, T, T&, T*, KeyOfT, Hash> iterator;
	typedef HashNode<T> Node;
	typedef HashTable<K, T, KeyOfT, Hash> HT;

	Node* _node;
	const HT* _pHashTable;//指向哈希表的指针

	__HashTableIterator(Node* node, const HT* pHashTable)
		:_node(node)
		, _pHashTable(pHashTable)
	{}

	//普通迭代器时，它时拷贝构造
	//const迭代器时，它是构造
	__HashTableIterator(const iterator& it)
		:_node(it._node)
		, _pHashTable(it._pHashTable)
	{}

	Ref operator*()
	{
		return _node->_data;
	}

	Ptr operator->()
	{
		return &_node->_data;
	}

	Self operator++()
	{
		//当前桶还未遍历结束
		if (_node->_next)
		{
			_node = _node->_next;
		}
		else
		{
			//此时，当前的桶已经遍历完了，需要对下一个桶进行遍历
			//1、计算当前桶的哈希值
			size_t hashi = _pHashTable->HashFunc(keyoft(_node->_data)) % _pHashTable->_tables.size();
			++hashi;//++i进入到下一个哈希桶
			for (; hashi < _pHashTable->_tables.size(); ++hashi)
			{
				Node* cur = _pHashTable->_tables[hashi];
				if (cur)
				{
					_node = cur;
					return *this;
				}
			}

			_node = nullptr;
		}
		return *this;
	}

	bool operator!=(const Self& s)
	{
		return _node != s._node;
	}
};

//如果K是一个整型，则直接返回该数据
template<class K>
struct _Hash
{
	const K& operator()(const K& key)
	{
		return key;
	}
};

//特化
template<>
struct _Hash<string>
{
	size_t operator()(const string& key)
	{
		size_t hash = 0;
		for (size_t i = 0; i < key.size(); ++i)
		{
			hash *= 131;
			hash += key[i];
		}
		return hash;
	}
};

template<class K, class T, class KeyOfT, class Hash>
class HashTable
{
	typedef HashNode<T> Node;

	template<class K, class T, class Ref, class Ptr, class KeyOfT, class HashFunc>
	friend struct __HashTableIterator;
public:
	typedef __HashTableIterator<K, T, T&, T*, KeyOfT, Hash> iterator;
	typedef __HashTableIterator<K, T, const T&, const T*, KeyOfT, Hash> const_iterator;

	iterator begin()
	{
		for (size_t i = 0; i < _tables.size(); ++i)
		{
			if (_tables[i])
			{
				return iterator(_tables[i], this);
			}
		}

		return end();
	}

	iterator end()
	{
		return iterator(nullptr, this);
	}

	const_iterator begin() const
	{
		for (size_t i = 0; i < _tables.size(); ++i)
		{
			if (_tables[i])
			{
				return const_iterator(_tables[i], this);
			}
		}

		return end();
	}

	const_iterator end() const
	{
		return const_iterator(nullptr, this);
	}

	//析构函数
	~HashTable()
	{
		Clear();
	}

	void Clear()
	{
		for (size_t i = 0; i < _tables.size(); ++i)
		{
			Node* cur = _tables[i];
			while (cur)
			{
				Node* next = cur->_next;
				delete cur;
				cur = next;
			}
			_tables[i] = nullptr;
		}
	}

	size_t HashFunc(const K& key) const
	{
		Hash hash;
		return hash(key);
	}

	size_t GetNextPrime(size_t num)
	{
		const int PRIMECOUNT = 28;
		static const size_t primeList[PRIMECOUNT] =
		{
			53ul, 97ul, 193ul, 389ul, 769ul,
			1543ul, 3079ul, 6151ul, 12289ul, 24593ul,
			49157ul, 98317ul, 196613ul, 393241ul, 786433ul,
			1572869ul, 3145739ul, 6291469ul, 12582917ul,25165843ul,
			50331653ul, 100663319ul, 201326611ul, 402653189ul,805306457ul,
			1610612741ul, 3221225473ul, 4294967291ul
		};

		for (size_t i = 0; i < PRIMECOUNT; ++i)
		{
			if (primeList[i] > num)
			{
				return primeList[i];
			}
		}
		return primeList[PRIMECOUNT - 1];
	}

	pair<iterator, bool> Insert(const T& data)
	{
		KeyOfT keyoft;

		iterator it = Find(keyoft(data));
		if (it != end())
			return make_pair(it, false);

		if (_num == _tables.size())
		{
			//创建一个新表，并扩容
			vector<Node*> newtables;
			//size_t newsize = _tables.size() == 0 ? 10 : _tables.size() * 2;
			size_t newsize = GetNextPrime(_tables.size());
			newtables.resize(newsize);

			//将旧表中的数据拷贝到新表中
			for (size_t i = 0; i < _tables.size(); ++i)
			{
				Node* cur = _tables[i];
				while (cur)
				{
					size_t index = HashFunc(keyoft(data)) % newtables.size();
					Node* next = cur->_next;
					cur->_next = newtables[index];
					newtables[index] = cur;

					cur = next;
				}
				_tables[i] = nullptr;
			}
			_tables.swap(newtables);
		}

		//正常先查看该数据在不在表中，如果在就返回false，代码前面已经查找过了，
		//这里就不再查询了。 

		//计算数据在表中映射的位置
		size_t index = HashFunc(keyoft(data)) % _tables.size();

		//将该数据头查到链表中
		Node* newnode = new Node(data);
		newnode->_next = _tables[index];
		_tables[index] = newnode;

		++_num;
		return make_pair(iterator(newnode, this), true);
	}

	iterator Find(const K& key)
	{
		if (_tables.size() == 0)
			return iterator(nullptr, this);

		KeyOfT keyoft;
		size_t index = HashFunc(key) % _tables.size();
		Node* cur = _tables[index];

		while (cur)
		{
			if (keyoft(cur->_data) == key)
			{
				return iterator(cur, this);
			}
			else
			{
				cur = cur->_next;
			}
		}

		return iterator(nullptr, this);
	}

	bool Erase(const K& key)
	{
		if (Find(key) == nullptr)
			return false;

		KeyOfT keyoft;
		size_t index = HashFunc(key) % _tables.size();
		Node* prev = nullptr;
		Node* cur = _tables[index];

		while (cur)
		{
			if (keyoft(cur->_data) == key)
			{
				if (prev == nullptr)
				{
					//此时，说明要删除的节点就是链表的第一个节点
					_tables[index] = cur->_next;
				}
				else
				{
					prev->_next = cur->_next;
				}
				delete cur;

				return true;
			}
			else
			{
				prev = cur;
				cur = cur->_next;
			}
		}
		return nullptr;
	}

private:
	vector<Node*> _tables;
	size_t _num;//记录哈希表中存储的数据的个数
};



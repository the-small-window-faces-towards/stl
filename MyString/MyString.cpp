#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
#include<assert.h>
using namespace std;

class MyString
{
public:
	typedef char* iterator;
	typedef const char* const_iterator;
public:
	//1、构造函数
	MyString(const char*str="")
		:_size(strlen(str))//先_size后_capacity是为了匹配声明顺序，防止给成随机值
	{
		_capacity = _size == 0 ? 2 : _size;
		_str = new char[_capacity + 1];//这里多开一个空间给结束符'\0'留空间
		strcpy(_str, str);
	}

	//2、拷贝构造--string s2(s1)
	//1）传统写法
	/*MyString(const MyString& str)
		:_str(new char[strlen(str._str) + 1])
		, _size(str._size)
		, _capacity(str._capacity)
	{
		strcpy(_str, str._str);
	}*/

	//2)拷贝构造现在常用写法
	MyString(const MyString& str)
		:_str(nullptr)
		,_size(0)
		,_capacity(0)
	{
		MyString tmp(str._str);//使用构造函数创建一个临时对象
		swap(tmp);
	}


	//3、赋值运算符重载 s1=s2
	//1）传统写法
	/*MyString& operator=(const MyString& str)
	{
		if (this != &str)
		{
			char* tmp = new char[strlen(str._str) + 1];
			strcpy(tmp, str._str);

			delete[] _str;
			_str = tmp;
			_capacity = str._capacity;
			_size = str._size;
		}
		return *this;
	}*/

	//2)赋值的现代常用写法s1=s2
	MyString operator=(MyString s)
	{
		swap(s);

		return *this;
	}

	~MyString()
	{
		delete[] _str;
		_str = nullptr;
		_size = _capacity = 0;
	}

	void swap(MyString& s)
	{
		//用::指定调用全局的swap函数即库里的swap函数
		::swap(_str, s._str);
		::swap(_size, s._size);
		::swap(_capacity, s._capacity);
	}

	//size()
	size_t size() const
	{
		return _size;
	}

	size_t capacity() const
	{
		return _capacity;
	}

	char& operator[](size_t i)
	{
		assert(i < _size);
		return _str[i];
	}

	//operator[]
	const char& operator[](size_t i) const
	{
		assert(i < _size);
		return _str[i];
	}

	//对象以字符串的形式返回
	const char* c_str()
	{
		return _str;
	}

	//迭代器
	iterator begin()
	{
		return _str;
	}

	iterator end()
	{
		return _str + _size;
	}

	const_iterator begin() const
	{
		return _str;
	}

	const_iterator end() const
	{
		return _str + _size;
	}


	//在字符串尾部插入一个字符
	void push_back(const char ch)
	{
		//首先判断字符串空间是否已满，如果满了则需要扩容
		if (_size == _capacity)
		{
			size_t newCapacity = _capacity==0 ? 2 : 2 * _capacity;
			reserve(newCapacity);
		}

		_str[_size] = ch;
		++_size;
		_str[_size] = '\0';
	}

	//尾插一个字符串
	void append(const char* str)
	{
		size_t len = strlen(str);
		if (_size + len > _capacity)
		{
			reserve(_size+len);
		}

		strcpy(_str + _size, str);
		_size += len;
	}

	//s1+='a'
	MyString& operator+=(const char ch)
	{
		push_back(ch);

		return *this;
	}

	//s1+="abcd"
	MyString& operator+=(const char* s)
	{
		append(s);

		return *this;
	}

	//在指定位置插入一个字符，并返回新字符串
	MyString& insert(size_t pos, const char ch)
	{
		assert(pos <= _size);
		if (_size == _capacity)
		{
			size_t newCapacity = _capacity == 0 ? 2 : 2 * _capacity;
			reserve(newCapacity);
		}

		//以下写法是错误的，为什么呢？
		/*size_t end = _size;
		while (end >= pos)
		{
			_str[end + 1] = _str[end];
			--end;
		}*/
		//为什么不能这样写呢？
		//当进行头插时，pos==0，此时end==0，执行到代码最后--end为-1，
		//由于end为无符号整型，故为-1时，即为size_t类型的最大值，此时会进入死循环。
		//那能不能把pos换成int类型？也不能，
		//pos为有符号int类型，end为无符号整型，两者进行比较时，pos会被提升为无符号整型，也进入死循环。

		size_t end = _size + 1;
		while (end > pos)
		{
			_str[end] = _str[end - 1];
			--end;
		}

		_str[pos] = ch;
		++_size;

		return *this;
	}

	//在指定位置插入一个字符串
	MyString& insert(size_t pos, const char* str)
	{
		assert(pos <= _size);

		size_t len = strlen(str);
		if (_size + len > _capacity)
		{
			reserve(_size + len);
		}

		size_t end = _size + len;
		while (pos+len-1 < end)
		{
			_str[end] = _str[end-len];
			--end;
		}

		strncpy(_str + pos, str,len);
		_size += len;

		return *this;
	}

	//erase()删除指定位置往后len个字符，并返回新字符串
	MyString& erase(size_t pos, size_t len = npos)
	{
		assert(pos < _size);

		if (len == npos || pos + len > _size)
		{
			_str[pos] = '\0';
			_size = pos;
		}
		else
		{
			//写法1：
			/*size_t i = pos + len;
			while (i<=_size)
			{
				_str[i-len] = _str[i];
				++i;
			}
			_size -= len;*/

			//写法2：
			strcpy(_str + pos, _str + pos + len);
			_size -= len;
		}

		return *this;
	}

	//find--查找字符
	size_t find(char ch, size_t pos = 0) const
	{
		assert(pos < _size);

		size_t i = pos;
		while (i < _size)
		{
			if (_str[i] == ch)
			{
				return i;
			}
			++i;
		}

		return npos;
	}

	//find--查找字符串
	size_t find(const char* str, size_t pos = 0) const
	{
		assert(pos < _size);

		char* p = strstr(_str+pos, str);
		if (p == nullptr)
		{
			return npos;
		}
		else
		{
			return p - _str;
		}
	}


	//将字符串容量扩展到n
	void reserve(size_t n)
	{
		if (n > _capacity)
		{
			char* tmp = new char[n + 1];
			strcpy(tmp, _str);
			delete[] _str;
			_str = tmp;
			_capacity = n;
		}
	}

	//resize(size_t n,char ch='\0')
	void resize(size_t n, char ch = '\0')
	{
		if (n < _size)
		{
			_str[_size] = '\0';
			_size = n;
		}
		else
		{
			if (n > _capacity)
			{
				reserve(n);
			}

			for (size_t i = _size; i < n; ++i)
			{
				_str[i] = ch;
			}

			_size = n;
			_str[_size] = '\0';
		}
	}

	//s1<s2
	bool operator<(const MyString& s)
	{
		return strcmp(_str, s._str) > 0;
	}

	//s1==s2
	bool operator==(const MyString& s)
	{
		return strcmp(_str, s._str) == 0;
	}

	//s1<=s2
	bool operator<=(const MyString& s)
	{
		return *this < s || *this == s;
	}

	bool operator>(const MyString& s)
	{
		return !(*this <= s);
	}

	bool operator>=(const MyString& s)
	{
		return !(*this < s);
	}

	bool operator!=(const MyString& s)
	{
		return !(*this == s);
	}

	//clear--将字符串清空
	void clear()
	{
		_str[0] = '\0';
		_size = 0;
	}


private:
	char* _str;
	size_t _size;
	size_t _capacity;
	static const size_t npos = -1;
};

//输出函数<<
ostream& operator<<(ostream& out, const MyString& str)
{
	for (int i = 0; i < str.size(); ++i)
	{
		cout << str[i];
	}
	return out;
}

//输入
istream& operator>>(istream& in, MyString& s)
{
	while (1)
	{
		char ch;
		//in >> ch;
		ch = in.get();
		if (ch == ' ' || ch == '\n')
		{
			break;
		}
		else
		{
			s += ch;
		}
	}
	return in;
}


void test_MyString01()
{
	MyString s1;
	MyString s2("hello");

	cout << s1 << endl;
	cout << s2 << endl;

	cout << s1.c_str() << endl;
	cout << s2.c_str() << endl;

	for (size_t i = 0; i < s1.size(); ++i)
	{
		cout << s1[i] << " ";
	}
	cout << endl;

	for (size_t i = 0; i < s2.size(); ++i)
	{
		cout << s2[i] << " ";
	}
	cout << endl;

	MyString::iterator it = s2.begin();
	while (it != s2.end())
	{
		*it -= 1;
		cout << *it << " ";
		++it;
	}
	cout << endl;
}

void test_MyString02()
{
	MyString s1("hello");
	s1.push_back(' ');
	s1.push_back('w');
	s1.append("orld xxxxxxx");
	cout << s1 << endl;

	MyString s2;
	s2 += "hello";
	s2 += ' ';
	s2 += "world";
	cout << s2 << endl;

	s2.insert(1, 'A');
	cout << s2 << endl;
	s2.insert(4, "CSC");
	s2.insert(0, "ABS");
	cout << s2 << endl;
}

void test_MyString03()
{
	MyString s1("hello");
	s1.reserve(10);
	cout << s1.size() << " ";
	cout << s1.capacity() << " ";
	cout << s1 << endl;

	s1.resize(8, 'x');
	cout << s1.size() << " ";
	cout << s1.capacity() << " ";
	cout << s1 << endl;

	s1.resize(18, 'a');
	cout << s1.size() << " ";
	cout << s1.capacity() << " ";
	cout << s1 << endl;

	s1.erase(5, 3);
	cout << s1 << endl;

	cout << s1.find("llo", 2) << endl;

	MyString s2;

	cin >> s2;
	cout << s2 << endl;
}




int main()
{
	test_MyString03();
	return 0;
}
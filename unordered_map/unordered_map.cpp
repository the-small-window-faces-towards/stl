#include"HashTable.h"

template<class K, class V, class Hash=_Hash<K>>
class MyUnorderedMap
{
	struct MapKeyOfT
	{
		const K& operator()(const pair<K, V>& kv)
		{
			return kv.first;
		}
	};

public:
	typedef typename HashTable<K, pair<const K, V>, MapKeyOfT, Hash>::iterator iterator;
	typedef typename HashTable<K, pair<const K, V>, MapKeyOfT, Hash>::const_iterator const_iterator;

	iterator begin()
	{
		return _ht.begin();
	}

	iterator end()
	{
		return _ht.end();
	}

	const_iterator begin() const
	{
		return _ht.begin();
	}

	const_iterator end() const
	{
		return _ht.end();
	}

	pair<iterator,bool> Insert(const pair<K, V>& kv)
	{
		return _ht.Insert(kv);
	}

	V& operator[](const K& key)
	{
		pair<iterator, bool> ret = _ht.Insert(make_pair(key, V()));
		return ret.first->second;
	}


private:
	HashTable<K, pair<const K, V>, MapKeyOfT, Hash> _ht;
};

void test_MyUnorderedMap()
{
	MyUnorderedMap<string, string> MyMap;
	MyMap.Insert(make_pair("string", "�ַ���"));
	MyMap.Insert(make_pair("sort", "����"));
	MyMap.Insert(make_pair("int", "����"));
	MyMap["left"] = "���";
	MyMap["right"] = "�ұ�";

	auto it = MyMap.begin();
	while (it != MyMap.end())
	{
		cout << it->first << ":" << it->second << endl;
		++it;
	}
}



int main()
{
	test_MyUnorderedMap();
	return 0;
}





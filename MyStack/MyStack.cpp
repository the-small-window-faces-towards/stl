#include<iostream>
#include<stack>
#include<vector>
#include<deque>
#include<list>
#include <stdexcept> // 用于异常处理

//stack--C++ 标准库中的基本用法
void test_stack()
{
	std::stack<int> stack;
	int sum(0);

	//1、push()--将元素压入栈中
	for (int i = 0; i < 10; ++i)
		stack.push(i);

	//2、size()--返回栈中元素的个数
	std::cout << stack.size() << std::endl;

	//3、emplace()--在栈顶部添加一个新元素，该元素位于其当前顶部元素的上方。
	stack.emplace(11);

	std::cout << stack.size() << std::endl;

	//4、empty()--判断栈是否为空
	//5、top()--返回栈顶元素的引用
	//6、pop()--将栈顶元素弹出
	while (!stack.empty())
	{
		//取栈顶的数据
		std::cout << stack.top() << " ";
		stack.pop();//栈顶元素出栈
	}
	std::cout << std::endl;

	std::cout << stack.size() << std::endl;
}

//新增一个容器的模板参数，T是数据的类型，
//Container是一个容器的类型(容器具体是什么类型，可以根据需要传入)。
template<class T,class Container=std::deque<T>>
class MyStack
{
public:
	void push(const T& val)
	{
		_con.push_back(val);
	}

	void pop()
	{
		if (!empty())
		{
			_con.pop_back();
		}
		else
		{
			throw std::runtime_error("Stack is empty.");
		}
	}

	T& top()
	{
		if (!empty())
		{
			return _con.back();
		}
		else
		{
			throw std::runtime_error("Stack is empty.");
		}
	}

	size_t size()
	{
		return _con.size();
	}


	bool empty ()
	{
		return _con.empty();
	}

private:
	Container _con;
};


void test_MyStack()
{
	//MyStack<int, vector<int>> st;
	MyStack<int, std::deque<int>> stack;
	stack.push(1);
	stack.push(2);
	stack.push(3);
	stack.push(4);

	while (!stack.empty())
	{
		std::cout << stack.top() << " ";
		stack.pop();
	}
	std::cout << std::endl;
}


int main()
{
	test_stack();
	test_MyStack();

	return 0;
}
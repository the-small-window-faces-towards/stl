#include<iostream>
#include<string>
using namespace std;

//string类的成员函数及其基本用法
//1、构造函数
void constructor_test()
{
	string s1;//构造一个空字符串
	string s2("hello world");//使用一个常量字符串来构造出一个string类对象
	string s3(s2);//拷贝构造

	//在string类中已经重载了流插入<< 和流提取>> 运算符，故可以直接使用<<来输出string字符串对象
	cout << "字符串s1：" << s1 << endl;
	cout << "字符串s2：" << s2 << endl;
	cout << "字符串s3：" << s3 << endl;
}


//2、operator= 赋值运算符重载
void test_1()
{
	string s1, s2, s3;

	s1 = "hello world";
	s2 = 'c';
	s3 = s1;

	cout << "s1字符串为：" << s1 << endl;
	cout << "s2字符串为：" << s2 << endl;
	cout << "s3字符串为：" << s3 << endl;
}

//3、string类对象的容量操作
//(1)size--返回字符串有效字符的长度
void test_2()
{
	string s1;
	string s2("hello world");

	cout << "size of s1：" << s1.size() << endl;
	cout << "size of s2：" << s2.size() << endl;
}

//(2)capacity--获取最大容量
void test_3()
{
	string s1;
	string s2("hello world hello everyone");

	cout << "capacity of s1：" << s1.capacity() << endl;
	cout << "capacity of s2：" << s2.capacity() << endl;
}

//(4)clear--清空有效字符，
//   resize--将有效字符的个数该成n个，多出的空间用字符c填充
void test_4()
{
	string s("hello world!");
	cout << "size of s:" << s.size() << endl;
	cout <<"capacity of s:" << s.capacity() << endl;
	cout << s << endl;
	cout << "============" << endl;

	//将s中的字符串清空，注意清空时只是将size清零，不改变底层空间的大小
	s.clear();
	cout << "size of s:" << s.size() << endl;
	cout << "capacity of s:" << s.capacity() << endl;
	cout << "============" << endl;

	//将s中的有效字符个数增加到10个，多出的位置用字符'a'进行填充
	//"aaaaaaaaaa"
	s.resize(10, 'a');
	cout << "size of s:" << s.size() << endl;
	cout << "capacity of s:" << s.capacity() << endl;
	cout << s << endl;
	cout << "============" << endl;

	//将s中有效字符个数增加到15个，多出位置用缺省值'\0'进行填充
	//"aaaaaaaaaa\0\0\0\0\0"
	//此时s中有效字符个数以及增加到了15个
	s.resize(15);
	cout << "size of s:" << s.size() << endl;
	cout << "capacity of s:" << s.capacity() << endl;
	cout << s << endl;
	cout << "============" << endl;

	//将s中的有效字符个数缩小到5个
	s.resize(5);
	cout << "size of s:" << s.size() << endl;
	cout << "capacity of s:" << s.capacity() << endl;
	cout << s << endl;
}

//4、reserve--为字符串预留空间
void test_5()
{
	string s1("hello world!");
	string s2 = s1;

	cout << "the capacity of s1:" << s1.capacity() << endl;
	s1.reserve(100);
	cout << "after reserve(100),the capacity of s1:" << s1.capacity() << endl;
	s1.reserve(50);
	cout << "after reserve(50),the capacity of s1:" << s1.capacity() << endl;

	cout << "the capacity of s2:" << s2.capacity() << endl;
	s2.reserve(1);
	cout << "after reserve(1),the capacity of s2:" << s2.capacity() << endl;
}

//5、operator[] / at()
void test_6()
{
	string s("hello world");
	cout << s << endl;
	for (int i = 0; i < s.size(); ++i)
		cout << s[i] << ' ';
	cout << endl;

	for (int i = 0; i < s.size(); ++i)
		cout << s.at(i) << ' ';
	cout << endl;
}

//6、迭代器iterator--begin()/end()
void test_7()
{
	string s("hello world!");
	string::iterator it = s.begin();
	while (it != s.end())
	{
		(*it)++;
		cout << *it <<" ";
		it++;
	}
	cout << endl;
}

//7、rbegin()/end()
void test_8()
{
	string s("hello world!");
	string::reverse_iterator rit = s.rbegin();
	while (rit != s.rend())
	{
		cout << *rit << " ";
		++rit;
	}
	cout << endl;
}

//8、插入(拼接)方式：push_back  append  operator+=
void test_9()
{
	string str;
	str.push_back(' ');//在str后插入空格
	str.append("hello");//在str后追加一个字符"hello"
	str += 'w';//在str后追加一个字符'b'
	str += "orld";//在str后追加一个字符串"it"
	cout << str << endl;
}

//9、erase
void test_10()
{
	string str("hello world");

	str.erase(2, 2);
	cout << str << endl;

	str.erase(1);
	cout << str << endl;
}

//10、正向和反向查找：find/rfind
void test_11()
{
	//获取file的后缀
	string file("string.cpp");
	size_t pos = file.rfind('.');
	string suffix(file.substr(pos, file.size() - pos));
	cout << suffix << endl;

	//取出url中的域名
	string url("https://legacy.cplusplus.com/reference/string/string/find/");
	cout << url << endl;
	size_t start = url.find("://");
	if (start == string::npos)
	{
		cout << "invalid url" << endl;
		return;
	}
	start += 3;
	size_t finish = url.find('/', start);
	string address = url.substr(start, finish - start);
	cout << address << endl;

	//删除url的协议前缀
	pos = url.find("://");
	url.erase(0, pos + 3);
	cout << url << endl;
}

//11、c_str
void test_12()
{
	string s("hello world");
	const char* str = s.c_str();

	cout << str << endl;
}

//12、substr
void test_13()
{
	string s1("hello world");
	string s2 = s1.substr(0, 5);//hello
	string s3 = s1.substr(5);// world

	cout << s2 << s3 << endl;
}

//13、operator<</operator>>
void test_14()
{
	string s1, s2;

	cin >> s1;
	cin >> s2;

	cout << "s1 = " << s1 << endl;
	cout << "s2 = " << s2 << endl;
}

//14、getline()
void test_15()
{
	string s1;
	getline(cin, s1);

	cout << s1 << endl;
}


int main()
{
	test_15();

	return 0;
}